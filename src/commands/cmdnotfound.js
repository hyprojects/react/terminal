const cmdnotfound = function(argv) {
    return `Command ${argv[0]} does not exist. Type help for more options.`
}

export default cmdnotfound

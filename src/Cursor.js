import React from 'react'
import './Cursor.css'

function Cursor(props) {
    return (
        <span className="cursor">knowledgearc.io:/archive$</span>
    )
}

export default Cursor

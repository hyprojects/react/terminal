import React from 'react'
import Cursor from './Cursor'

function Cmd(props) {
    return (
        <li key={props.i}>
            {props.entry.stdIn ? <Cursor /> : ""}
            {props.entry.cmd}
        </li>
    )
}

export default Cmd

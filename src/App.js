import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import logo from './logo.svg';
import './App.css'
import Cmd from './Cmd'
import Cursor from './Cursor'

import {help, cmdnotfound} from './commands'

function App() {
    const [terminal, setTerminal] = useState([])
    const [command, setCommand] = useState("")
    const [output, setOutput] = useState("")
    const [pending, setPending] = useState(false)

    const cmdRef = useRef();

    const list = async (args) => {
        setPending(true)
        setOutput("loading...")

        try {
            const response = await fetch('https://demo.archive.knowledgearc.net/knowledgearc-rest/collections.json')
            const json = await response.json()
        } catch (e) {
            setOutput("sorry it didn't work out")
            console.log(e)
        }

        setPending(false)
    }

    const commands = {
        "list": list,
        "help": help,
        "cmdnotfound": cmdnotfound
    }

    const keyPressed = e => {
        if (e.key === "Enter") {
            console.log("you typed ", e.target.value)

            if (e.target.value === "clear") {
                setTerminal([])
            } else {
                var cmd = commands[e.target.value] ? e.target.value : "cmdnotfound"
                var ret = commands[cmd]

                if (typeof ret === "function") {
                    ret(e.target.value.split(" "))
                }

                setTerminal(
                    [
                        ...terminal,
                        {
                            cmd: cmd,
                            stdIn: true
                        }
                    ]
                )

                setCommand(cmd)
            }

            e.target.value = ""
        }
    }

    useEffect(() => {
        if (command) {
            setTerminal(
                [
                    ...terminal,
                    {
                        cmd: output,
                        stdIn: false
                    }
                ]
            )
        }
    }, [output])

    useLayoutEffect(() => {
        const { current } = cmdRef

        current.scrollIntoView({ behavior: "smooth" })
    }, [terminal])

    useLayoutEffect(() => {
        console.log("pending", pending)
        const { current } = cmdRef

        if (pending) {
            current.disabled = true
        } else {
            current.disabled = false
        }
    }, [pending])

    var h1 = `
 _                        _          _
| | ___ __   _____      _| | ___  __| | __ _  ___  __ _ _ __ ___
| |/ / '_ \\ / _ \\ \\ /\\ / / |/ _ \\/ _\` |/ _\` |/ _ \\/ _\` | '__/ __|
|   <| | | | (_) \\ V  V /| |  __/ (_| | (_| |  __/ (_| | | | (__
|_|\\_\\_| |_|\\___/ \\_/\\_/ |_|\\___|\\__,_|\\__, |\\___|\\__,_|_|  \\___|
   _ __   ___| |___      _____  _ __| ||___/
  | '_ \\ / _ \\ __\\ \\ /\\ / / _ \\| '__| |/ /
 _| | | |  __/ |_ \\ V  V / (_) | |  |   <
(_)_| |_|\\___|\\__| \\_/\\_/ \\___/|_|  |_|\\_\\`

const h2 = `
--------------------------------------------------------------------------------
The decentralized archiving ecosystem
https://knowledgearc.io/
Token: Archive (ARCH) 0xC3c14CF95a4F97E18C8c1F9EA8dA7Ad4E8F81331
--------------------------------------------------------------------------------

Type help for more options.`

    return (
        <main className="App">
            <h1>{h1}</h1>
            <h2>{h2}</h2>
            <ul>
                {terminal.map((entry, i) => <Cmd key={i} entry={entry} />)}
            </ul>
            {!pending ? <Cursor /> : ""}
            <input
                type="text"
                ref={cmdRef}
                onKeyPress={keyPressed}
                autoFocus />
        </main>
    )
}

export default App;
